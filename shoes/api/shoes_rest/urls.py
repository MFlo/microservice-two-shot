from django.urls import path
from .views import api_list_shoes, api_show_shoe

urlpatterns = [
    # URL for creating a new shoe or listing all shoes
    path("shoes/", api_list_shoes, name="api_create_shoes"),

    # URL for listing shoes filtered by a specific bin
    # 'bin_vo_id' is a variable part of the URL, capturing an integer to identify the bin
    path("bins/<int:bin_vo_id>/shoes/", api_list_shoes, name="api_list_shoes"),

    # URL for showing, updating, or deleting a specific shoe
    # 'pk' (primary key) is a variable part of the URL, capturing an integer to identify the shoe
    path("shoes/<int:pk>/", api_show_shoe, name="api_show_shoe"),
]
