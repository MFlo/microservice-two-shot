from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    # Unique reference for importing from an external API
    import_href = models.CharField(max_length=200, unique=True)

    # Name of the closet where the bin is located
    closet_name = models.CharField(max_length=200)

    # Number assigned to the bin
    bin_number = models.PositiveSmallIntegerField()
class Shoe(models.Model):
    #Shoe model should have manufacturer, model name, color, picture URL,
    #and associated bin in the wardrobe.

    # Brand or maker of the shoe
    manufacturer = models.CharField(max_length=200)

    # Name or model of the shoe
    model_name = models.CharField(max_length=200)

    # Color of the shoe
    color = models.CharField(max_length=200)

    # URL for the shoe's image
    picture_url = models.CharField(max_length=200)

    # ForeignKey relationship to the BinVO model
    # 'related_name' is used for reverse querying from BinVO to Shoe
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    # String representation of the object
    def __str__(self):
        return self.name

    # Method to get the API URL for the shoe instance
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
