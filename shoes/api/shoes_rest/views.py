from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import BinVO, Shoe

# Encoder for detailed BinVO data
class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "import_href"]
# Encoder for listing Shoe objects
class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    # Extra data to include the bin's closet name for each shoe
    def get_extra_data(self, o):
        return {"bin" : o.bin.closet_name}
    # Specify encoder for the BinVO field
    encoders = {
        "bin": BinVOEncoder(),
    }

# View function to handle GET and POST requests for the list of shoes
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder = ShoeListEncoder,
            safe=False,
        )
    else:
        # Create a new shoe
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href = bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )

# View function to handle GET, DELETE, and PUT requests for a single shoe
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_shoe(request, pk):
    if request.method == "GET":
        try:
            # Retrieve and return a single shoe
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
        )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Exist the shoe does not"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse({"message": "Shoe be gone"})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Exist the shoe does not"})
        # Delete a shoe and return success status
            # count, _ = Shoe.objects.filter(id=pk).delete()
            # return JsonResponse({"deleted": count > 0})
    else:
        # Update a shoe
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=pk)

            props = ["manufacturer", "model_name", "color", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
        )
        except Shoe.DoesNotExist:
            response = JsonResponse({"message": "Exist it does not"})
            response.status_code = 404
            return response
