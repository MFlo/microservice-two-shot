import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# from shoes_rest.models import Something
from shoes_rest.models import BinVO
# Import models from shoes_rest, here.
def get_bins():
    # Gets bins data from the Wardrobe API
    # and updates or creates BinVA objects
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    # send GET request to the wardrobe API to get bins data
    content = json.loads(response.content)

    # Loop through each bin in response and update/create
    # BinVO objects
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            defaults={
                "closet_name": bin["closet_name"],
                "bin_number": bin["bin_number"]
            }
        )

def poll():
    # continuously poll for new data from the Wardrobe API
    # at regular invervals
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            get_bins()
            # pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    # run the polling process if the script is executed
    # as the main program
    poll()
