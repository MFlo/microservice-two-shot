import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function HatList() {
    const [HatColumns, setHatColumns] = useState([[],[],[]]);

    async function getHats() {
        const url = 'http://localhost:8090/api/hats';

        try {
            const response = await fetch(url);
            if(response.ok) {
                const data = await response.json();

                const requests = [];
                for (let hat of data.hats) {
                    const detailUrl = `http://localhost:8090${hat.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);
                const HatColumns = [[],[],[]];

                let i = 0;
                for (const HatResponse of responses) {
                    if(HatResponse.ok) {
                        const details = await HatResponse.json();
                        HatColumns[i].push(details);
                        i += 1;
                        if(i>2) {
                            i=0;
                        }
                    } else {
                        console.error(HatResponse);
                    }
                }
                console.log(HatColumns)
                setHatColumns(HatColumns);
            }
        } catch (e) {
            console.error(e);
        }
    }
    useEffect(() => {
        getHats();
    }, []);

    return (
    <>
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">HATSIFY</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          All of your shoes in one place!
        </p>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/create" className="btn btn-primary btn-lg px-4 gap-3">Add a hat</Link>
        </div>
    </div>
    </div>
    <div className="container">
        <div className="row">
            {HatColumns.map((HatList, index) => {
                return (
                    <HatColumn key={index} list={HatList} />
                );
            })}
        </div>
    </div>
    </>
    );
}
function HatColumn(props) {
    const DeleteHat = async (href) => {
        console.log(href);
        const url= `http://localhost:8090/${href}`;

        const fetchConfig = {
            method: "DELETE",
            headers: {'Content-Type': 'application/json'},
        };
        const response = await fetch(url,fetchConfig);
        if (response.ok) {
            window.location.reload();
        }
    }
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                console.log("THE prop IS OVER HERE:", props);
                console.log("the data is over here", data);
                return (
                    <>
                    <div key={hat.href} className="card mb-3 shadow">
                        <img
                            src={hat.image}
                            className="card-img-top"
                            alt='Hat Image'
                        />
                        <div className="card-body">
                            <h5 className="card-title text-center">{hat.color}, {hat.fabric}, {hat.style} hat</h5>
                            <h6 className="card-subtitle mb-2 text-muted text-center">
                                {hat.location}
                            </h6>
                            <button onClick={() => DeleteHat(hat.href)} className="btn btn-outline-danger text-center">Delete</button>
                        </div>
                    </div>
                    </>
                )})}
            </div>
        )
}






export default HatList;
