import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatList from './HatList';

// Main App component, serves as the root of the application
function App() {
  return (
    // BrowserRouter for routing in the app
    <BrowserRouter>
      <Nav /> {/* Navigation Bar component */}
      <div className="container">
        {/* Routes define various navigation paths and their corresponding components */}
        <Routes>
          <Route path="/" element={<MainPage />} /> {/* Home/Main page route */}
          {/* Nested Routes for Shoes */}
          <Route path="shoes">
            <Route index element={<ShoeList />} /> {/* Default route for '/shoes', displays the list of shoes */}
            <Route path="new" element={<ShoeForm />} /> {/* Route for adding a new shoe */}
          </Route>
          {/* Nested Routes for Hats */}
          <Route path="hats">
            <Route index element={<HatList />} /> {/* Default route for '/hats', displays the list of hats */}
            <Route path="create" element={<HatForm />} /> {/* Route for adding a new hat */}
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
