import React, { useState, useEffect } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        fabric: '',
        style: '',
        image: '',
        location: ''
    })

    const getData = async () => {
        const url = "http://localhost:8100/api/locations/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8090/api/hats/create/';

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        console.log("form data", JSON.stringify(formData));

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                fabric: '',
                style: '',
                color: '',
                image: '',
                location: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({...formData, [inputName]: value});
    }
    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Add a new hat</h1>
            <form onSubmit={handleSubmit} id="add-hat-form">
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                    <label htmlFor="color">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.style} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                    <label htmlFor="style">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} value={formData.image} placeholder="Image" required type="text" name="image" id="image" className="form-control" />
                    <label htmlFor="image">Image</label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleFormChange} value={formData.location} required name="location" id="location" className="form-select" >
                        <option value="">Choose a location</option>
                        {locations.map(location => {
                            return (
                            <option key={location.href} value={location.href}>{location.closet_name}</option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Add</button>
            </form>
            </div>
        </div>
    </div>
    );
}

export default HatForm;
