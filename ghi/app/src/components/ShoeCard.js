// import React from 'react';

// const ShoeCard = ({ shoe }) => {
//   // A function to handle shoe deletion, to be implemented.
//   const handleDelete = (shoeId) => {
//     // Here you would call the API to delete the shoe and then update the state of your ShoeList component.
//     console.log(`Delete shoe with ID: ${shoeId}`);
//     // fetch(`/api/shoes/${shoeId}`, { method: 'DELETE' })
//     // .then(response => response.json())
//     // .then(data => {
//     //   // Handle the successful deletion (e.g., remove shoe from the list in state)
//     // })
//     // .catch(error => console.error('Error deleting shoe:', error));
//   };

//   return (
//     <div className="shoe-card">
//       <img src={shoe.picture_url} alt={shoe.model_name} className="shoe-image" />
//       <div className="shoe-details">
//         <h3 className="shoe-model-name">{shoe.model_name}</h3>
//         <p className="shoe-manufacturer">Manufactured by: {shoe.manufacturer}</p>
//         <p className="shoe-color">Color: {shoe.color}</p>
//         <p className="shoe-bin">Bin: {shoe.bin}</p>
//         {/* If the bin is an object with more details, you might need to adjust the above line */}
//       </div>
//       <button onClick={() => handleDelete(shoe.id)} className="delete-shoe-btn">
//         Delete Shoe
//       </button>
//     </div>
//   );
// };

// export default ShoeCard;
