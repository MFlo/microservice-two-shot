// import React, { useState, useEffect } from 'react';
// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import ShoeCard from './components/ShoeCard';
// import CreateShoeForm from './components/CreateShoeForm';

// const ShoeList = () => {
//   const [shoes, setShoes] = useState([]);
//   // State to control the display of the shoe creation form
//   const [showCreateForm, setShowCreateForm] = useState(false);

//   useEffect(() => {
//     fetch('/api/shoes/')
//       .then(response => response.json())
//       .then(data => setShoes(data.shoes))
//       .catch(error => console.error('Error fetching shoes:', error));
//   }, []);

//   // Function to toggle the shoe creation form
//   const toggleCreateForm = () => {
//     setShowCreateForm(!showCreateForm);
//   };

//   // Function to handle shoe deletion
//   const handleDelete = (shoeId) => {
//     fetch(`/api/shoes/${shoeId}/`, { method: 'DELETE' })
//       .then(() => {
//         // Filter out the shoe that was deleted
//         const updatedShoes = shoes.filter(shoe => shoe.id !== shoeId);
//         setShoes(updatedShoes);
//       })
//       .catch(error => console.error('Error deleting shoe:', error));
//   };

//   return (
//     <div>
//       <button onClick={toggleCreateForm} className="create-shoe-btn">
//         {showCreateForm ? 'Cancel' : 'Create New Shoe'}
//       </button>

//       {showCreateForm && <CreateShoeForm />}

//       <ul>
//         {shoes.map(shoe => (
//           <li key={shoe.id}>
//             <ShoeCard shoe={shoe} />
//             <button onClick={() => handleDelete(shoe.id)} className="delete-shoe-btn">
//               Delete Shoe
//             </button>
//           </li>
//         ))}
//       </ul>
//     </div>
//   );
// };

// export default ShoeList;
