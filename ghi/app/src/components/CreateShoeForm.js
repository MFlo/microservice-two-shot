// import React, { useState } from 'react';

// const CreateShoeForm = ({ addShoeToList }) => {
//   const [formData, setFormData] = useState({
//     manufacturer: '',
//     model_name: '',
//     color: '',
//     picture_url: '',
//     bin: ''
//   });

//   // Function to handle form submission
//   const handleSubmit = (event) => {
//     event.preventDefault();
//     // Here you would call the API to create the shoe and then update the state of your ShoeList component
//     console.log('Form data submitted:', formData);

//     // Reset the form
//     setFormData({
//       manufacturer: '',
//       model_name: '',
//       color: '',
//       picture_url: '',
//       bin: ''
//     });

//     // Optionally, call a prop method to add the new shoe to the list in the parent component
//     // addShoeToList(newShoe);
//   };

//   // Function to handle changes in form inputs
//   const handleChange = (event) => {
//     const { name, value } = event.target;
//     setFormData(prevFormData => ({
//       ...prevFormData,
//       [name]: value
//     }));
//   };

//   return (
//     <form onSubmit={handleSubmit}>
//       {/* Form fields for manufacturer, model_name, color, picture_url, and bin */}
//       {/* Each input should have an onChange handler to update state with handleChange */}
//       {/* Include a submit button */}
//     </form>
//   );
// };

// export default CreateShoeForm;
