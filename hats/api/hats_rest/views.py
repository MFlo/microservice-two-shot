from django.shortcuts import render
from .models import Hat, LocationVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model: LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style",
        "color",
        "image",
        "location"
    ]
    def get_extra_data(self, o) :
        return {"location" : o.location.closet_name}
    # encoders = {
    #     "location": LocationVOEncoder,
    # }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        print("OVER HERE!!!!!!!! ", hats)
        return JsonResponse(
            {"hats": hats},
            encoder = HatListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        print("VLVBNLDJHVBNLEJRHGV", content)

        try:
            location_href = content["location"]
            print("LOCATION HREF HERE",location_href)
            location = LocationVO.objects.get(import_href = location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder= HatListEncoder,
            safe= False,
        )

@require_http_methods(["DELETE","GET","PUT"])
def api_show_hat(request,pk):
    """
    Single-object API for Hat resource.

    GET:
    Returns the details for a Hat resouce based on the value of pk.
    {
        "fabric":
        "style":
        "color":
        "image":
        "location":
    }

    PUT:
    Updates the Hat resource information based on the value of the pk
    {
        "fabric"
        "style"
        "color"
        "image"
    }

    DELETE:
    Removes the Hat resource from the application.
    """
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatListEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "That hat does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse({"message":"Hat deleted."})
        except Hat.DoesNotExist:
            return JsonResponse({"message": "That hat does not exist"})
    else: #PUT
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)

            props = ["fabric","style","color","image"]
            for prop in props:
                if prop in content:
                    setattr(hat,prop,content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatListEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
