# Wardrobify

Team:

* Person 1 - Michael Flores Which microservice? Shoes
* Person 2 - Angelika Villapando Which microservice? Hats

## Design

## Shoes microservice
Key Components of the Shoes Section:

Shoes API:
Create a Django application for the Shoes API.
Implement models, views, and URLs.
This API should handle operations to list shoes, create new shoes, and delete shoes.

Shoes Poller:
Develop a polling script to pull data from the Wardrobe API about Bin resources.
This data will be used by your Shoes API.

React Front-End:
Create components for listing shoes and showing their details.
Develop a form component for adding new shoes.
Provide functionality to delete shoes.
Route navigation links to the components.

Explain your models and integration with the wardrobe
microservice, here.

Project Overview:

Wardrobify: An application designed for organizing hats and shoes in multiple closets.
Objective: Create RESTful APIs in microservices, integrate these services, and develop a React front-end.

My Role:
Focus on Shoes microservice: creation of the Shoes API, Shoes Poller, and React components for shoes with further integration to the Hats microservice.

## Hats microservice
Key Components of the Hats Section:

Hats API:
Create a Django application for the Hats API.
Implement models, views, and URLs.
This API should handle operations to list Hats, create new Hats, and delete Hats.

Hats Poller:
Develop a polling script to pull data from the Wardrobe API about Bin resources.
This data will be used by your Shoes API.

React Front-End:
Create components for listing Hats and showing their details.
Develop a form component for adding new Hats.
Provide functionality to delete Hats.
Route navigation links to the components.

Explain your models and integration with the wardrobe
microservice, here.

Project Overview:

Wardrobify: An application designed for organizing hats and shoes in multiple closets.
Objective: Create RESTful APIs in microservices, integrate these services, and develop a React front-end.

My Role:
Focus on Hats microservice: creation of the Hats API, Hats Poller, and React components for Hats with further integration to the Shoes microservice.
